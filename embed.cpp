#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// Dump a file to C code for embedding into a binary.
// Usage: embedfile <input> <output> <symbol>
int main(int argc, char* argv[])
{
	if(argc < 4)
		return 0;

	FILE *in = fopen(argv[1], "rb");
	fseek(in, 0, SEEK_END);
	long size = ftell(in);
	fseek(in, 0, SEEK_SET);
	unsigned char *data = new unsigned char[size];
	fread(data, size, 1, in);
	fclose(in);

	char buf[4096];
	FILE *out = fopen(argv[2], "wb");
	sprintf(buf, "const unsigned int %s_size = 0x%lX;\nconst unsigned char %s[] = {\n", argv[3], size, argv[3]);
	fwrite(buf, strlen(buf), 1, out);
	for(long i = 0;i < size;i += 16)
	{
		memset(buf, 0, sizeof(buf));
		for(long j = 0;j < 16;++j)
		{
			char hex[5];
			sprintf(hex, "0x%02X", data[i+j]);
			strcat(buf, hex);
			if(i+j+1 < size)
				strcat(buf, ",");
			else
				break;
		}
		strcat(buf, "\n");
		fwrite(buf, strlen(buf), 1, out);
	}
	fwrite("};\n", 3, 1, out);
	fclose(out);

	delete[] data;
	return 0;
}
